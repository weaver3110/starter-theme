<!DOCTYPE html>
<html class="no-js desktop-layout" <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php the_field('favicon','option'); ?>">
		<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#000">
		<?php wp_head(); ?>
		<?php echo get_field('head_scripts', 'options'); ?>
	</head>
	<body <?php body_class(); ?>>
		<?php $logo = get_bloginfo( 'stylesheet_directory' ) . '/assets/images/logo.png'; ?>
		<?php echo get_field('body_scripts', 'options'); ?>
		<header class="global-header" id="global-header">
			<nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light">
				<a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo $logo; ?>" alt="<?php bloginfo( 'name' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"></a>
				<!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mainNavigation" aria-controls="mainNavigation" aria-expanded="false" aria-label="Toggle navigation"> -->
				<!-- <span class="navbar-toggler-icon"></span> -->
				<ul class="navbar-toggler-icon">
					<li class="header-nav-item hamburger">
	                	<a  class="navbar-toggler icon" data-toggle="collapse" data-target="#mainNavigation" aria-controls="mainNavigation" aria-expanded="false" aria-label="Toggle navigation" href="javascript:void(0);" class="icon" onclick="mobileNav()">
		                    <div class="bars">
		                        <span></span>
		                        <span></span>
		                        <span></span>
		                        <span></span>
		                    </div>
		                </a>
		            </li>
				</ul>
				<!-- </button> -->
				<?php
			      wp_nav_menu([
			        'menu'            => 'Main Navigation',
			        'theme_location'  => 'top',
			        'container'       => 'div',
			        'container_id'    => 'mainNavigation',
			        'container_class' => 'collapse navbar-collapse',
			        'menu_id'         => false,
			        'menu_class'      => 'navbar-nav ml-auto',
			        'depth'           => 3,
			        'fallback_cb'     => 'bs4navwalker::fallback',
			        'walker'          => new bs4navwalker()
			      ]);
			    ?>
			    <?php
			    get_search_form(); 
			    ?>
			</nav>
			<div class="clearfix"></div>
		</header>