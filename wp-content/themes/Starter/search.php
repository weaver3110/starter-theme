<?php
/**
 * The template for displaying search results pages
 *
 */

get_header(); ?>

<section id="search-results" class="content-area no-width-wrapper">
	<div class="container" role="div">
<?php
$args = array(
	's' => get_search_query(),
	'post_type' => array( 'post', 'page', 'tb_videos', 'tb_downloads' ),
);
$query = new WP_Query( $args );
// The Loop
if ( $query->have_posts() ) {
	$total_results = $query->post_count;
	echo '<div class="result_count">'.$total_results.' Results</div>';
	echo '<div class="row search_tabs">';
		echo '<div class="col-md-12">'; 
			echo '<a href="#" data-show="show--block" class="active">'._('All').'</a>';
			echo '<a href="#" data-show="show--post">'._('Articles').'</a>';
			echo '<a href="#" data-show="show--tb_videos">'._('Videos').'</a>';
			echo '<a href="#" data-show="show--page">'._('Pages').'</a>';
			echo '<a href="#" data-show="show--tb_downloads">'._('Downloads').'</a>';
		echo '</div>';
	echo '</div>';
	echo '<div class="row">';
	echo '<div class="no_result">No results for this post type found.</div>';
    while ( $query->have_posts() ) {
        $query->the_post();
		echo '<div class="col-md-4 show--block show--'.get_post_type().'">'; 
			include(locate_template('template-parts/include--'.get_post_type().'.php'));
		echo '</div>';
    }
    echo '</div>';
} else { // no posts found ?>
	
	<div class="row">
		<div class="col-sm-12 show--block text-center">
			<div class="h3"><?php echo _('No matches found, please try again.') ?></div>
		</div>
	</div>

<?php }
/* Restore original Post Data */
wp_reset_postdata();

?>
		</div>
	</section>
	
<?php get_footer(); ?>