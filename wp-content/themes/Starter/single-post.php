<?php
get_header();
?>
<?php
    if ( have_posts() ) {
        while ( have_posts() ) { ?>
        <div class="wrapper single-post">
            <?php  echo get_the_post_thumbnail( $post->ID, 'hero', array( 'class' => 'featured' ) );?>
            <section class="single-content">
                <div class="container">
                    <h1 class="text-center"><?php the_title(); ?></h1>
                    <?php
                        the_post(); 
                        the_content();
                    ?>
                </div>
            </section>
        </div>
    <?php } // end while
} // end if
?>
<?php get_footer(); ?>