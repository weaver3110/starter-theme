<?php
/**
 * WP Starter Theme functions and definitions
 *
 * Sets up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 */

/***************************************************
 * Definitions
 ***************************************************/
define( 'CLIENT_URI', get_stylesheet_directory_uri() );
define( 'CLIENT_PATH', get_stylesheet_directory() );
define( 'CLIENT_ASSETS', 'assets' );
define( 'CLIENT_JS', 'js' );
define( 'CLIENT_CSS', 'css' );

define( 'FULL_CLIENT_JS_PATH', CLIENT_URI . '/' . CLIENT_ASSETS . '/' . CLIENT_JS . '/');
define( 'FULL_CLIENT_CSS_PATH', CLIENT_URI . '/' . CLIENT_ASSETS . '/' . CLIENT_CSS . '/');

/***************************************************
 * Functions
 ***************************************************/
include_once( 'functions/custom_post_types.php' );       // custom post types and taxonomies
include_once('functions/bs4navwalker.php');				 // site menus
include_once('functions/block-functions.php');           // blocks
include_once('functions/acf-gravity-forms-field.php');   // Gravity Forms dropdown for ACF
include_once('functions/acf-modifications.php');        // Field Modifications for ACF (background color consistency, etc.)

/**
 * Enqueue scripts and styles
 */
 
function def6_theme_scripts() {

/* CSS */
    wp_enqueue_style( 'style-main', FULL_CLIENT_CSS_PATH . 'style.css' );

/* JS */
    wp_enqueue_script( 'script-main', FULL_CLIENT_JS_PATH . 'main.js', array('jquery'), true );
    wp_enqueue_script( 'script-bootstrap', CLIENT_URI . '/assets/vendor/bootstrap/js/bootstrap.min.js', array('jquery'), '1.0.0', true );

}
add_action( 'wp_enqueue_scripts', 'def6_theme_scripts' );

// ACF options page(s)
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
}

/**************************************************
 * Set up theme defaults and registers support
 * for various WordPress features.
 ***************************************************/
/*
 * Let WordPress manage the document title.
 * By adding theme support, we declare that this theme does not use a
 * hard-coded <title> tag in the document head, and expect WordPress to
 * provide it for us.
 */
add_theme_support( 'title-tag' );

/**
 * Add excerpt support to pages
 */
add_post_type_support( 'page', 'excerpt' );

/**
 * Add featured images
 */
add_theme_support( 'post-thumbnails' );

add_image_size( 'alternating', 550, 550, true ); // 550 pixels wide by 550 pixels tall, hard crop mode
add_image_size( 'hero', 2400, 550, true ); // 2400 pixels wide by 550 pixels tall, hard crop mode

/*
 * Switch default core markup for search form, comment form, and comments
 * to output valid HTML5.
 */
add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) );
/**
 * Add Menus
 */
add_theme_support('menus');

// Check if the menu exists
$menu_name = 'Main Navigation';
$menu_exists = wp_get_nav_menu_object( $menu_name );

// If it doesn't exist, let's create it.
if( !$menu_exists){
    $menu_id = wp_create_nav_menu($menu_name);

	// Set up default menu items
    wp_update_nav_menu_item($menu_id, 0, array(
        'menu-item-title' =>  __('Home'),
        'menu-item-classes' => 'home',
        'menu-item-url' => home_url( '/' ), 
        'menu-item-status' => 'publish'));

    wp_update_nav_menu_item($menu_id, 0, array(
        'menu-item-title' =>  __('Custom Page'),
        'menu-item-url' => home_url( '/custom/' ), 
        'menu-item-status' => 'publish'));

}

// allow svg's to be added to media library
function d6_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'd6_mime_types');

//edit mime type names
function d6_get_mime_for_attachment( $post_id )
{
    $type = get_post_mime_type( $post_id );

    if( ! $type )
        return 'N/A';

    switch( $type )
    {
        case 'image/jpeg':
        case 'image/png':
        case 'image/gif':
        case 'image/svg+xml':
        case 'image/eps':
        case 'image/tiff':
        case 'image/ai':
            return "image";

        case 'video/mpeg':
        case 'video/mp4': 
        case 'video/mov': 
        case 'video/quicktime':
            return "video";

        case 'text/csv':
        case 'text/plain': 
        case 'text/xml':
            return "text";

        case 'application/pdf':
            return "pdf";

        case 'application/rvt':
            return "RVT";

        case 'application/dwg':
            return "DWG";

        case 'application/skp':
            return "SKP";

        case 'application/dwf':
            return "DWF";

        default:
            return "file";
    }
}
//change the width of Gutenberg Blocks in Admin
function custom_admin_css() {
    echo '<style type="text/css">
    .wp-block { max-width: 90%; }
    </style>';
}
add_action('admin_head', 'custom_admin_css');

// hide unexplained errors until wp releases core fix.
function warning_squelch_wpe(int $errno , string $errstr , string $errfile , int $errline , array $errcontext) {
  if(strstr($errstr, "expected to be a reference")) {
    return true; // squelch matching warnings
  }
  // allow normal handling for non-matching warnings
  return false;
}
set_error_handler("warning_squelch_wpe", E_WARNING);