<?php 
if( function_exists('acf_add_local_field_group') ):

    //Add background color options consistently throughout the admin to the "background_color" select field
    function bg_color_field_choices( $field ) {

        $field['choices'] = array (
            'white-bg' => 'White',
            'green-bg' => 'Green',
            'gold-yellow-bg' => 'Golden Yellow',
            'orange-bg' => 'Orange',
            'lt-blue-bg' => 'Blue (Light)',
            'blue-bg' => 'Blue',
            'dk-blue-bg' => 'Blue (Dark)',
            'tb-blue-bg' => 'Blue (Darkest)',
            'gray-bg' => 'Gray',
        );

        return $field;
    }
    add_filter('acf/load_field/name=background_color', 'bg_color_field_choices');
endif;
?>