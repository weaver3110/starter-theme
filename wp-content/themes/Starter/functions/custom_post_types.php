<?php 

/* 
* CUSTOM POST TYPES
*/
function video_custom_init() {
    $args = array(
      'public' => true,
      'label'  => 'Videos',
      'menu_icon' => 'dashicons-video-alt2',
      'show_in_rest' => true,
      'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt')
    );
    register_post_type( 'tb_videos', $args );
}
add_action( 'init', 'video_custom_init' );

function download_custom_init() {
    $args = array(
      'public' => true,
      'label'  => 'Downloads',
      'menu_icon' => 'dashicons-download',
      'show_in_rest' => true,
      'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt')
    );
    register_post_type( 'tb_downloads', $args );
}
add_action( 'init', 'download_custom_init' );

function donor_custom_init() {
    $args = array(
      'public' => true,
      'label'  => 'Donors',
      'menu_icon' => 'dashicons-awards',
      'has_archive' => true,
      'show_in_rest' => true,
      'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt')
    );
    register_post_type( 'tb_donors', $args );
}
add_action( 'init', 'donor_custom_init' );

function bio_custom_init() {
    $args = array(
      'public' => true,
      'label'  => 'Bios',
      'menu_icon' => 'dashicons-groups',
      'show_in_rest' => true,
      'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt')
    );
    register_post_type( 'tb_bios', $args );
}
add_action( 'init', 'bio_custom_init' );

/* 
* CUSTOM TAXONOMIES
*/
add_action( 'init', 'create_video_category_taxonomies', 0 );

function create_video_category_taxonomies() {
	// Add new taxonomy, make it hierarchical (like categories)
	$args = array(
		'hierarchical'      => true,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'video_category' ),
	);

    register_taxonomy( 'video_category', array( 'tb_videos' ), $args );
}

add_action( 'init', 'create_download_category_taxonomies', 0 );

function create_download_category_taxonomies() {
	// Add new taxonomy, make it hierarchical (like categories)
	$args = array(
		'hierarchical'      => true,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'download_category' ),
	);

    register_taxonomy( 'download_category', array( 'tb_downloads' ), $args );
}

add_action( 'init', 'create_donor_category_taxonomies', 0 );

function create_donor_category_taxonomies() {
	// Add new taxonomy, make it hierarchical (like categories)
	$args = array(
		'hierarchical'      => true,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'donor_category' ),
	);

    register_taxonomy( 'donor_category', array( 'tb_donors' ), $args );
}

add_action( 'init', 'create_bio_category_taxonomies', 0 );

function create_bio_category_taxonomies() {
	// Add new taxonomy, make it hierarchical (like categories)
	$args = array(
		'hierarchical'      => true,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'bio_category' ),
	);

    register_taxonomy( 'bio_category', array( 'tb_bios' ), $args );
}
?>