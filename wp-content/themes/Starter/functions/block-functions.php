<?php

// render new blocks
function my_acf_block_render_callback( $block ) {
	// convert name ("acf/testimonial") into path friendly slug ("testimonial")
	$slug = str_replace('acf/', '', $block['name']);
	// include a template part from within the "template-parts/block" folder
	if( file_exists( get_theme_file_path("/template-parts/blocks/{$slug}.php") ) ) {
		include( get_theme_file_path("/template-parts/blocks/{$slug}.php") );
	}
}


// Add new block categories
function def6_block_category( $categories, $post ) {
	return array_merge(
		$categories,
		array(
			array(
				'slug' => 'bootstrap',
				'title' => __( 'Bootstrap Blocks', 'bootstrap' ),
            ),
            array(
				'slug' => 'custom',
				'title' => __( 'Custom Blocks', 'custom' ),
			),
		)
	);
}
add_filter( 'block_categories', 'def6_block_category', 10, 2);





// add new blocks
add_action('acf/init', 'my_acf_init');
function my_acf_init() {
	
	// check function exists
	if( function_exists('acf_register_block') ) {
		
		// register a Custom blocks
		acf_register_block(array(
			'name'				=> 'hero',
			'title'				=> __('Hero Component'),
			'description'		=> __('Hero Image.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'custom',
			'icon'				=> 'format-image',
			'keywords'			=> array( 'hero', 'header' ),
		));
		acf_register_block(array(
			'name'				=> 'content',
			'title'				=> __('Content Component'),
			'description'		=> __('Content Component.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'custom',
			'icon'				=> 'editor-alignleft',
			'keywords'			=> array( 'content' ),
		));
		acf_register_block(array(
			'name'				=> 'banner',
			'title'				=> __('Banner Component'),
			'description'		=> __('Banner Image.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'custom',
			'icon'				=> 'align-center',
			'keywords'			=> array( 'banner' ),
		));
		acf_register_block(array(
			'name'				=> 'alternating',
			'title'				=> __('Alternating Component'),
			'description'		=> __('Alternating Component.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'custom',
			'icon'				=> 'forms',
			'keywords'			=> array( 'alternating' ),
		));
		acf_register_block(array(
			'name'				=> 'one_third',
			'title'				=> __('One-Third Component'),
			'description'		=> __('One-Third Component.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'custom',
			'icon'				=> 'excerpt-view',
			'keywords'			=> array( 'one', 'third', 'one third', 'one-third' ),
		));
		acf_register_block(array(
			'name'				=> 'accordion',
			'title'				=> __('Accordion Component'),
			'description'		=> __('Accordion Component.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'custom',
			'icon'				=> 'list-view',
			'keywords'			=> array( 'accordion', 'faq', 'collapse' ),
		));
		acf_register_block(array(
			'name'				=> 'logo',
			'title'				=> __('Logo Component'),
			'description'		=> __('Logo Component.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'custom',
			'icon'				=> 'star-filled',
			'keywords'			=> array( 'logo', 'sponsor', 'donor', 'logos', 'sponsors', 'donors' ),
		));
		acf_register_block(array(
			'name'				=> 'two_column',
			'title'				=> __('Two Column Component'),
			'description'		=> __('Two Column Component.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'custom',
			'icon'				=> 'screenoptions',
			'keywords'			=> array( 'two', 'column' ),
		));
		acf_register_block(array(
			'name'				=> 'three_column',
			'title'				=> __('Three Column Component'),
			'description'		=> __('Three Column Component.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'custom',
			'icon'				=> 'editor-insertmore',
			'keywords'			=> array( 'three', 'column' ),
		));
		acf_register_block(array(
			'name'				=> 'social',
			'title'				=> __('Social Component'),
			'description'		=> __('Social Component.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'custom',
			'icon'				=> 'share',
			'keywords'			=> array( 'social' ),
		));
		acf_register_block(array(
			'name'				=> 'tabbed',
			'title'				=> __('Tabbed Component'),
			'description'		=> __('Tabbed Component.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'custom',
			'icon'				=> 'category',
			'keywords'			=> array( 'tab', 'tabs', 'tabbed' ),
		));
		acf_register_block(array(
			'name'				=> 'map',
			'title'				=> __('Map Component'),
			'description'		=> __('Map Component.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'custom',
			'icon'				=> 'location-alt',
			'keywords'			=> array( 'tab', 'tabs', 'map' ),
		));
	}
}