<div class="search-form searchVis">
	<form class="searchbox" id="headerSearch" name="search_bar" role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    	<input type="submit" class="search-form__submit" value="<?php echo get_search_query() ?>">  
	    <span class="searchbox-icon">
	      <i class="fa fa-search icon-search" aria-hidden="true"></i>
	    </span>
	    <input class="search-form__input" id="hs" type="search" placeholder="<?php echo esc_attr_x( 'Search...', 'placeholder' ) ?>" name="s" onkeyup="buttonUp();" value="" required>
	</form>
</div>

