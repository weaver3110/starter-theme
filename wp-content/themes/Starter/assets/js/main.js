jQuery(function($) {
    /* Get accurate Viewport sizing (no jQuery) */
    var viewportwidth;
    var viewportheight;

    function getWindowSize() {
        if (typeof window.innerWidth != 'undefined') {
            viewportwidth = window.innerWidth,
                viewportheight = window.innerHeight
        } else if (typeof document.documentElement != 'undefined' && typeof document.documentElement.clientWidth != 'undefined' && document.documentElement.clientWidth != 0) {
            viewportwidth = document.documentElement.clientWidth,
                viewportheight = document.documentElement.clientHeight
        } else {
            viewportwidth = document.getElementsByTagName('body')[0].clientWidth,
                viewportheight = document.getElementsByTagName('body')[0].clientHeight
        }
    }

    /* Global Vars */
    var $window = $(window),
        $document = $(document),
        scrolled = false


    /******
    Start Run Functions
    ******/

    $document.ready(function() {
        getWindowSize();
        //Open links in new window
        $("a[href^='http:']:not([href*='" + window.location.host + "']), a[href^='https:']:not([href*='" + window.location.host + "'])").each(function() {
            $(this).attr("target", "_blank");
        });
        $window.width($window.width());

        //Search
        $('.search_btn').on("click", function(e) {
            $('#headerSearch').submit();
        });

        var isOpen = false;
        $('.searchbox-icon').click(function() {
            if (isOpen == false) {
                $('.navbar-nav').css('display', 'none');
                $('.searchbox').addClass('searchbox-open');
                $('.search-form').addClass('search-form-open');
                $('.search-form__input').focus();
                $('.searchbox-icon').addClass('searchbox-iconActive');
                isOpen = true;
            } else {
                $('.searchbox').removeClass('searchbox-open');
                $('.search-form').removeClass('search-form-open');
                $('.search-form__input').focusout();
                $('.searchbox-icon').removeClass('searchbox-iconActive');
                $('.navbar-nav').fadeIn();
                isOpen = false;
            }
        });
        $('.searchbox-icon').mouseup(function() {
            return false;
        });
        $('.searchbox').mouseup(function() {
            return false;
        });
        $document.mouseup(function() {
            if (isOpen == true) {
                $('.searchbox-icon').css('display', 'block');
                $('.searchbox-icon').click();
            }
        });

        $('#search-results .search_tabs a').click(function() {

            $('#search-results .search_tabs a').removeClass('active');
            $(this).addClass('active');
            var showThis = $(this).data('show');
            var numItems = $('.' + showThis).length;
            if (showThis != 'show--block') {
                $('.result_count').html(numItems + ' Results');
                $('#search-results .show--block').hide();
                $('#search-results .' + showThis).show();
            } else {
                $('.result_count').html(numItems + ' Results');
                $('.result_count').css('color', 'black');
                $('#search-results .' + showThis).show();
            }
            if (numItems == '0') {
                $('#search-results .no_result').show();
            } else {
                $('#search-results .no_result').hide();
            }
        })
        // END Search

        // Expanding descriptions for Topics landing page.
        $('.tab-content').on('click', '.tabbed-dd', getText);

        function getText(e) {
            e.preventDefault();

            $('.tabbed-description').remove();

            var collection = $('.tabbed-post-wrap');
            var num = 0;
            var num = $(this).closest('.tab-pane.active .tabbed-post-wrap').index('.tab-pane.active .tabbed-post-wrap');
            var numthird = ((num + 1) % 3);
            var numfirst = (num % 3);
            var id = $(this).attr("data-id");
            var imgsrc = $(this).attr("data-img-src");
            var imgalt = $(this).attr("data-img-alt");
            var imgtitle = $(this).attr("data-img-title");
            var title = $(this).attr("data-title");
            var jobtitle = $(this).attr("data-job-title");
            var content = $(this).attr("data-content");
            var newDiv = '<div class="tabbed-description slide" id="' + id + '"><div id="slideClose" class="close">X</div><div class="secondary_image"><img src="' + imgsrc + '" alt="' + imgalt + '" title="' + imgtitle + '"></div><div class="slide-content-wrap"><div class="text_title h4">' + title + '</div><div class="text_job_title h6">' + jobtitle + '</div><div class="text_content">' + content + '</div><div class="clearfix"></div></div></div>';
            var afterNum;

            isDesktop = true;

            if ($(this).hasClass('clicked')) {
                $(this).removeClass('clicked');
                $(this).closest('.tabbed-post-wrap').removeClass('tabbed-expanded-bg');
                $('.tabbed-description').remove();
                $('.tabbed-post-wrap').css('margin-bottom', '50px');

            } else {

                if ($(window).width() >= 576) {

                    if (numthird == 0) { //is third
                        afterNum = num;

                    } else {

                        if (num === $(this).closest('.tab-content').find('.tab-pane.active .tabbed-post-wrap').length - 1) {
                            afterNum = (parseFloat(num)); //is last
                        } else if (numfirst == 0) {
                            afterNum = (parseFloat(num) + 2); //is first
                        } else {
                            afterNum = (parseFloat(num) + 1); //is middle
                        }
                    }

                    $('.tab-pane.active .tabbed-post-wrap')[afterNum].insertAdjacentHTML('afterend', newDiv);
                    $('.tabbed-dd').removeClass('clicked');
                    $('.slide').removeClass('bio_open');
                    $(this).addClass('clicked');
                    $('.slide').addClass('bio_open');
                    $('.tabbed-post-wrap').removeClass('tabbed-expanded-bg');
                    $(this).closest('.tabbed-post-wrap').addClass('tabbed-expanded-bg');
                    $('.tabbed-post-wrap').css('margin-bottom', '0');

                } else {

                    afterNum = (parseFloat(num));
                    $('.tab-pane.active .tabbed-post-wrap')[afterNum].insertAdjacentHTML('afterend', newDiv);
                    $('.tabbed-dd').removeClass('clicked');
                    $('.slide').removeClass('bio_open');
                    $(this).addClass('clicked');
                    $('.slide').addClass('bio_open');
                    $('.tabbed-post-wrap').removeClass('tabbed-expanded-bg');
                    $(this).closest('.tabbed-post-wrap').addClass('tabbed-expanded-bg');
                    $('.tabbed-post-wrap').css('margin-bottom', '0');
                }
            }
        }

        $(document).on('click', '#slideClose', function() {
            $('.tabbed-description').remove();
            $('.tabbed-post-wrap').css('margin-bottom', '50px');
            $('.tabbed-dd').removeClass('clicked');
            $('.slide').removeClass('bio_open');
            $('.tabbed-post-wrap').removeClass('tabbed-expanded-bg');
        });

        // Mobile Navigation
        $('.hamburger').click(function() {
            $('.bars').toggleClass('open');
            $('.search-form').toggleClass('searchVis');
        });
        $('.menu-item-has-children span').click(function() {
            $(this).toggleClass('rotate');
            $(this).next('.dropdown-menu').toggleClass('show');
        });

        //Dropdown Functionality - Hover for Desktop
        if ($window.width() > 991) {
            function toggleDropdown(e) {
                const _d = $(e.target).closest('.dropdown'),
                    _m = $('.dropdown-menu', _d);
                setTimeout(function() {
                    const shouldOpen = e.type !== 'click' && _d.is(':hover');
                    _m.toggleClass('show', shouldOpen);
                    _d.toggleClass('show', shouldOpen);
                    $('[data-toggle="dropdown"]', _d).attr('aria-expanded', shouldOpen);
                }, e.type === 'mouseleave' ? 300 : 0);
            }

            $('body')
                .on('mouseenter mouseleave', '.dropdown', toggleDropdown)
                .on('click', '.dropdown-menu a', toggleDropdown);

            // Make parent of dropdown a clickable link
            $('.navbar .dropdown').hover(function() {
                $(this).find('.dropdown-menu').first().stop(true, true).delay(250).slideDown();

            }, function() {
                $(this).find('.dropdown-menu').first().stop(true, true).delay(100).slideUp();

            });

            $('.navbar .dropdown > a').click(function() {
                location.href = this.href;
            });
        }
        // Make parent of dropdown a clickable link mobile
        $('.navbar .dropdown span').click(function() {
            $(this).find('.dropdown-menu').first().stop(true, true).delay(250).slideDown();

        }, function() {
            $(this).find('.dropdown-menu').first().stop(true, true).delay(100).slideUp();

        });

        $('.navbar .dropdown > a').click(function() {
            location.href = this.href;
        });

        // end dropdown funtionality

    }); //end ready

    $window.on('load', function(event) {
        getWindowSize();
        $window.width($window.width());
    }); //end load

    $window.on('resize', function(event) {
        getWindowSize();
        $window.width($window.width());
    }); //end resize

    window.onscroll = function() {

    }; //end scroll

}); //end main $ function

function buttonUp() {
    var inputVal = jQuery('.search-form__input').val();
    var inputValCount = jQuery.trim(inputVal).length;
    if (inputValCount !== 0) {
        jQuery('.searchbox-icon').css('display', 'none');
        jQuery('.search-form__submit').attr("value", "GO");
    } else {
        jQuery('.search-form__input').val('');
        jQuery('.search-form__submit').attr("value", "");
        jQuery('.searchbox-icon').css('display', 'block');
    }
}