<?php 
/* template for pulling in a block style statically in a flex content block. */
$contentBlocks = get_sub_field('static_row');
if( have_rows('static_row') ):  ?>
<?php $blockCount = count($contentBlocks); ?>
	<div class="container">

	<?php while( have_rows('static_row') ): the_row(); 

		//vars
		$featured_image = get_sub_field('image');
		$featured_video = get_sub_field('video');
		$title = get_sub_field('header');
		$content = get_sub_field('content');
		$button = get_sub_field('button');
	?>

		<div class="col">

			<div class="include--single-wrap vertical_align">

				<?php if($featured_image): ?>
					<img src="<?php echo $featured_image['url']; ?>" alt="<?php echo $featured_image['alt']; ?>" title="<?php echo $featured_image['alt']; ?>" />
				<?php endif; ?>

				<?php if($title): ?>
					<div class="text_title"><?php echo $title; ?></div>
				<?php endif; ?>

				<?php if($content): ?>
					<div class="text_content"><?php echo $content; ?></div>
				<?php endif; ?>

				<?php if( $button ): ?>
					<div class="text_link">
						<a href="<?php echo $button['url']; ?>" target="<?php echo $button['target']; ?>" class="button feature--button"><?php echo $button['title']; ?></a>
					</div>
				<?php endif; ?>

			</div>

		</div>

	<?php  endwhile; ?>

	</div>
<?php wp_reset_postdata(); wp_reset_query(); ?>
<?php endif; ?>
