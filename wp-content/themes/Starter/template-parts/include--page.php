<?php /* Template for all places a page is included in a block/list style. */ 
//vars
$featured_image = get_the_post_thumbnail($post->ID, 'alternating');
$title = get_the_title($post->ID);
$content = get_the_excerpt($post->ID);
$button = get_permalink($post->ID);
$date = get_the_date();
?>
<a href="<?php echo $button; ?>" class="include--block accent-orange">
<span class="include--page">

    <?php if($featured_image): ?>
        <span class="featured_image">
            <?php echo $featured_image; ?>
        </span>
    <?php endif; ?>

    <span class="text_date"><?php echo $date; ?></span>

    <span class="text_title"><?php echo $title; ?></span>

</span>
</a>
<div class="clearfix"></div>