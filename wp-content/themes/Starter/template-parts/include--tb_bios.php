<?php /* Template for all places a bio post type is included in a block/list style. */
$featured_image = get_the_post_thumbnail($post->ID, 'alternating');
$secondary_image = get_field('secondary_image', $post->ID);
$title = get_the_title($post->ID);
$job_title = get_field('job_title', $post->ID);
$content = get_the_content($post->ID);
?>

<div class="include--bio">

    <?php if($featured_image): ?>
        <div class="featured_image"><?php echo $featured_image; ?></div>
    <?php endif; ?>

    <div class="text_title"><?php echo $title; ?></div>

    <?php if($job_title): ?>
        <div class="text_job_title"><?php echo $job_title; ?></div>
    <?php endif; ?>

    <div class="slide">
        
        <?php if($secondary_image): ?>
            <div class="secondary_image"><img src="<?php echo $secondary_image['url']; ?>" alt="<?php echo $secondary_image['alt']; ?>" title="<?php echo $secondary_image['title']; ?>"></div>
        <?php endif; ?>

        <?php if($title): ?>
            <div class="text_title"><?php echo $title; ?></div>
        <?php endif; ?>

        <?php if($job_title): ?>
            <div class="text_job_title"><?php echo $job_title; ?></div>
        <?php endif; ?>

        <?php if($content): ?>
            <div class="text_content"><?php echo $content; ?></div>
        <?php endif; ?>

    </div>

</div>

<div class="clearfix"></div>