
    <?php /* Template for all places a donor post type is included in a block/list style. */
    $imgID = $post->ID;
    $img = get_the_post_thumbnail_url($imgID);
    $imgAlt = '';
    $link = get_field('link', $imgID);

    if ($link) {
        $linkTitle = $link['title'];
        $linkTarget = $link['target'];
        $linkUrl = $link['url'];
    }
    echo '<div class="logo-img-container col-6 col-md-3">';
    if ($imgID && $link !== '') {
        echo '<a href="' . $linkUrl . '" target="' . $linkTarget . '">
            <span class="logo-img" style="background-image:url(' . $img . ');"></span>
        </a>';
    } else if ($imgID) {
        echo '<span class="logo-img" style="background-image:url(' . $img . ');"></span>';
    } 
    echo '</div>';

 ?>
