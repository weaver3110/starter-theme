<?php /* template for pulling in a block style manually in a flex content block. */ ?>

<div class="container">
	<div class="row">
		<?php 

		$manually_add_posts = get_sub_field('choose_posts_to_show');
		global $post;
		foreach( $manually_add_posts as $post):
			
			setup_postdata($post);
			$post_type = get_post_type();
			
			?>
				<div class="col" data-type="<?php echo $post_type; ?>_manual">

					<?php include(locate_template('template-parts/include--'.$post_type.'.php')); ?>
				</div>

		<?php endforeach; ?>
		<?php wp_reset_postdata(); wp_reset_query(); ?>
	</div>
</div>