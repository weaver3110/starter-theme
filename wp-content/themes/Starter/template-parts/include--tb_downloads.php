<?php /* Template for all places a download post type is included in a block/list style. */ 
$featured_image = get_the_post_thumbnail($post->ID, 'alternating');
$title = get_the_title($post->ID);
$content = get_the_excerpt($post->ID);
$file = get_field('file', $post->ID);
?>
<a href="<?php echo $file['url']; ?>" title="<?php echo $file['filename']; ?>" class="include--block accent-blue button--download" target="_blank">
<span class="include--download">

    <?php if($featured_image): ?>
        <span class="featured_image">
            <?php echo $featured_image; ?>
        </span>
    <?php endif; ?>

    <span class="text_title"><?php echo $title; ?></span>

    <?php if($content): ?>
        <span class="text_content"><?php echo $content; ?></span>
    <?php endif; ?>

</span>
</a>
<div class="clearfix"></div>