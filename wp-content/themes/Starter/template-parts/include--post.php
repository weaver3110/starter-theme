<?php /* Template for all places a post/article is included in a block/list style. */
global $post;
setup_postdata( $post );

$featured_image = get_the_post_thumbnail($post->ID, 'alternating');
$date = get_the_date('F j, Y', $post->ID);
$title = get_the_title($post->ID);
$content = get_the_excerpt($post->ID);
$button = get_permalink($post->ID);
?>
<a href="<?php echo $button; ?>" class="include--block accent-yellow">
<span class="include--post">

    <?php if($featured_image): ?>
        <span class="featured_image">
            <?php echo $featured_image; ?>
        </span>
    <?php endif; ?>

    <span class="text_date"><?php echo $date; ?></span>

    <span class="text_title"><?php echo $title; ?></span>

</span>
</a>
<div class="clearfix"></div>