<?php 
/* template for pulling in a block style automatically in a content block. */
	//vars
	$post_type = get_field('post_type');
	$args = array(
		'post_type' 		=> $post_type,
		'posts_per_page' 	=> '3',
		'orderby'			=> 'date'
	);
	$block_query = new WP_Query($args);
	global $post;
	$blockCount = $block_query->post_count;
	// The Loop
	if ( $block_query->have_posts() ) {
		echo '<div class="container">
			<div class="row">';
				while ( $block_query->have_posts() ) {
					$block_query->the_post();
					setup_postdata($post);
					$post_type == get_post_type();
				?>
					<div class="col" data-type="<?php echo $post_type; ?>_auto" data-position="<?php echo $blockCount; ?>">
						<?php include(locate_template('template-parts/include--'.$post_type.'.php')); ?>
					</div>

				<?php }
			echo '</div>
		</div>';
		/* Restore original Post Data */
		wp_reset_postdata(); wp_reset_query();
	} else {
		// no posts found
	}
 ?>