<?php /* Template for all places a videos post type is included in a block/list style. */ 
$featured_image = get_the_post_thumbnail($post->ID, 'alternating');
$date = get_the_date('F j, Y', $post->ID);
$title = get_the_title($post->ID);
$content = get_the_excerpt($post->ID);
$video = get_field('video', $post->ID);
?>
<a href="#" class="include--block accent-green">
<span class="include--video">

    <?php if($video): ?>
        <span class="featured_video">
            <?php echo $video; ?>
        </span>
    <?php endif; ?>

    <span class="text_date"><?php echo $date; ?></span>

    <span class="text_title"><?php echo $title; ?></span>
 
</span>
</a>
<div class="clearfix"></div>