
<?php
/**
 * Block Name: Alternating Component
 *
 * This is the template that displays the testimonial block.
 */
// create id attribute for specific styling
$id = 'block-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

?>
<section id="<?php echo $id; ?>" class="one-third">
	<div class="container">
		<?php if (get_field('title')): ?>
			<div class="title"><?php echo get_field('title'); ?></div>
		<?php endif ?>

		<?php if( have_rows('one_third') ):
			while ( have_rows('one_third') ) : the_row();
				$imgType = get_sub_field('image_type');
				$img = get_sub_field('image');
				$vid = get_sub_field('video');
				$title = get_sub_field('title');
				$alt = get_sub_field('one_third_copy');
				$link = get_sub_field('link');
				if($link){ 
					$linkTitle = $link['title'];
					$linkTarget = $link['target'];
					$linkUrl = $link['url'];
				}
				$imgUrl = $img['url'];
				$imgAlt = $img['alt'];
				echo '<div class="row one_thid_row">';
					echo '<div class="col-sm-12 col-md-4">';
						if ($imgType == 'image') {
							echo '<img src="' . $imgUrl . '" alt="' . $imgAlt . '"/>';
						} else {
							echo $vid;
						};
					echo '</div>';
					echo '<div class="col-sm-12 col-md-8">';
						echo '<div class="one-third-title">';
							if ($title) {
								echo '<h4>' . $title . '</h4>'; 
							}
						echo '</div>';
						echo '<div class="one-third-text">';
							if ($alt) {
								echo '<div class="one-third-text-container">' . $alt . '</div>'; 
							}
							if ($link) {
									echo '<a href="' . $linkUrl . '" target="' . $linkTarget . '" class="btn">' . $linkTitle . '</a>';
							}
						echo '</div>';
					echo '</div>';
				echo '</div>';
				echo '<div class="clearfix"></div>';
			endwhile;
		endif; ?>
		
	</div>
<div class="clearfix"></div>
</section>