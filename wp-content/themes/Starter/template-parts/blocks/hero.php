<?php
/**
 * Block Name: Hero Component
 *
 * This is the template that displays the testimonial block.
 */
// create id attribute for specific styling
$id = 'testimonial-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

?>
<section class="hero no_padding" id="<?php echo $id; ?>">
	<div id="carousel-<?php echo $id; ?>" class="carousel slide" data-ride="carousel">
		<?php
		$i = 0;
		$count = count(get_field('content'));
		if ($count > 1) {
		?>
	  		<ol class="carousel-indicators">
	    <?php
		    if( have_rows('content') ):
		        while ( have_rows('content') ) : the_row();
		        if($i == 0){
		            $class = 'class="active"';
		        }else{
		            $class = '';
		        }
		        ?>
		        <li data-target="#carousel-<?php echo $id; ?>" data-slide-to="<?php echo $i; ?>" <?php echo $class; ?>></li>
		        <?php
		        $i++;
		        endwhile;
		    endif;
		    ?>
		  </ol>
		<?php 
		}
		?>
	  	<div class="carousel-inner">
	  <?php
	    $i = 0;
	    if( have_rows('content') ):
	        while ( have_rows('content') ) : the_row();
	        if($i == 0){
	            $class = 'active';
	        }else{
	            $class = '';
	        }
	        $imgType = get_sub_field('image_type');
			$img = get_sub_field('image');
				if($img){
					// vars
					$imgUrl = $img['url'];
					$imgAlt = $img['alt'];
					$size = 'hero';
					$thumb = $img['sizes'][ $size ];
				}
	        $vid = get_sub_field('video');
	        $title = get_sub_field('title');
	        $link = get_sub_field('link');
	        	if ($link) {
		    		$linkTitle = $link['title'];
		    		$linkTarget = $link['target'];
		    		$linkUrl = $link['url'];
		    	}
	        
	        ?>
	        
	        <?php
	        	if ($imgType == 'image') {
	        ?>
		        	<div class="carousel-item <?php echo $class; ?>" style="background-image:url(<?php echo $imgUrl; ?>)">
		    <?php
		        } else {
					echo '<div class="carousel-item '.$class.'">';
	  				echo '<div class="hero_video_container"><video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop" class="hero_video">';
	    				echo '<source src="'.$vid.'" type="video/mp4">';
	  				echo '</video></div>';
		        }
		        if ($title) {
		    ?>
				<div class="carousel-caption d-none d-md-block">
			<?php 	
				if ($title) {
			?>
					<h5><?php echo $title; ?></h5>
			<?php
			}
				if ($link) {
			?>
					<a href="<?php echo $linkUrl; ?>" target="<?php echo $linkTarget;?>" class="btn"><?php echo $linkTitle; ?></a>
			<?php
			}
			?>
				</div>
			<?php
			}
			?>
	        </div>
	        <?php
	        $i++;
	        endwhile;
	    endif;
	    ?>
		</div>
		<?php 
		if ($count > 1) {
	  	?>
	  		<a class="carousel-control-prev" href="#carousel-<?php echo $id; ?>" role="button" data-slide="prev">
			    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
			    <span class="sr-only">Previous</span>
			</a>
			<a class="carousel-control-next" href="#carousel-<?php echo $id; ?>" role="button" data-slide="next">
			    <span class="carousel-control-next-icon" aria-hidden="true"></span>
			    <span class="sr-only">Next</span>
			</a>
		<?php
		}
	 	?>
	</div>

</section>