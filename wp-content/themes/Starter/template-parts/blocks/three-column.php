<?php
/**
 * Block Name: Three Column Component
 *
 * This is the template that displays the testimonial block.
 */
// create id attribute for specific styling
$id = 'block-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

?>
<section id="<?php echo $id; ?>" class="three-column">
	<div class="container">
		<?php if (get_field('title')): ?>
			<div class="title"><?php echo get_field('title'); ?></div>
		<?php endif ?>
		<div class="row">
			<?php 

			/***************
			 * *************
			 * AUTO
			 * *************
			****************/
			$importAuto = get_field('import_posts_automatically');

			if ($importAuto) {
				//vars
				$post_type = get_field('post_type');
				$args = array(
					'post_type' 		=> $post_type,
					'posts_per_page' 	=> '3',
					'orderby'			=> 'date'
				);
				$block_query = new WP_Query($args);
				global $post;
				$blockCount = $block_query->post_count;
				// The Loop
				if ( $block_query->have_posts() ) {
							while ( $block_query->have_posts() ) {
								$block_query->the_post();
								setup_postdata($post);
								$post_type == get_post_type();
							?>
								<div class="col-sm-12 col-md-4" data-type="<?php echo $post_type; ?>_auto" data-position="<?php echo $blockCount; ?>">
									<?php include(locate_template('template-parts/include--'.$post_type.'.php')); ?>
								</div>

							<?php }
					/* Restore original Post Data */
					wp_reset_postdata(); wp_reset_query();
				} else {
					// no posts found
				}
				
			} else {
				
				if( have_rows('three_column') ):
					while ( have_rows('three_column') ) : the_row();
						$importPosts = get_sub_field('import_from_posts');
						$imgType = get_sub_field('image_type');
						$img = get_sub_field('image');
						$vid = get_sub_field('video');
						$title = get_sub_field('title');
						$desc = get_sub_field('description');
						$link = get_sub_field('link');
						if ($link) {
							$linkTitle = $link['title'];
							$linkTarget = $link['target'];
							$linkUrl = $link['url'];
						}
						$imgUrl = $img['url'];
						$imgAlt = $img['alt'];
						$size = 'alternating';
						$thumb = $img['sizes'][ $size ];
						

						/***************
						 * *************
						 * Manual
						 * *************
						****************/
						if ($importPosts) {

							$manually_add_posts = get_sub_field('choose_posts_to_show');
							global $post;
							foreach( $manually_add_posts as $post):
								
								setup_postdata($post);
								$post_type = get_post_type();
								
								?>
									<div class="col-sm-12 col-md-4" data-type="<?php echo $post_type; ?>_manual">

										<?php include(locate_template('template-parts/include--'.$post_type.'.php')); ?>
									</div>

							<?php endforeach; ?>
							<?php wp_reset_postdata(); wp_reset_query(); 

						} else {
						/***************
						 * *************
						 * Static
						 * *************
						****************/

							echo '<div class="col-sm-12 col-md-4" data-type="static_auto">';
								echo '<a href="' . $linkUrl . '" target="' . $linkTarget . '" class="include--block accent-blue">';
									echo '<span class="include--page">';
										if ($imgType == 'image') {
											echo '<span class="featured_image">
													<img src="' . $thumb . '" alt="' . $imgAlt . '"/>
												</span>';
										} else { 
											echo '<span class="featured_video">';
												echo $vid;
											echo '</span>';
										}
										if ($title) {
											echo '<span class="text_title">'.$title.'</span>';
										}
										if ($desc) {
											echo '<span class="alt-text-container">' . $desc . '</span>'; 
										}
									echo '</span>';
								echo '</a>';
								echo '<div class="clearfix"></div>';
							echo '</div>';
						}
					endwhile;
				endif;
			}
			?>
		</div>
	</div>
	<div class="clearfix"></div>
</section>