<?php
/**
 * Block Name: Two Column Component
 *
 * This is the template that displays the testimonial block.
 */
// create id attribute for specific styling
$id = 'block-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

?>
<section id="<?php echo $id; ?>" class="two-column">
	<div class="container">
		<?php if (get_field('title')): ?>
			<div class="title"><?php echo get_field('title'); ?></div>
		<?php endif ?>
		<div class="row">
			<!-- markup php -->
			<?php 
				if( have_rows('two_column') ):
					while ( have_rows('two_column') ) : the_row();
						$importPosts = get_sub_field('import_from_posts');
						$img = get_sub_field('image');
						$title = get_sub_field('title');
						$link = get_sub_field('link');
						if ($link) {
							$linkTitle = $link['title'];
							$linkTarget = $link['target'];
							$linkUrl = $link['url'];
						}
						$imgUrl = $img['url'];
						$imgAlt = $img['alt'];

						if ($importPosts) {
							$manually_add_posts = get_sub_field('choose_posts_to_show');
							global $post;
							foreach( $manually_add_posts as $post):
								
								setup_postdata($post);
								$post_type = get_post_type();
								
								?>
								<div class="two-col-img-container col-md-6">
									<a href="<?php echo get_permalink(); ?>">
										<div class="two-col-img d-flex align-items-center" style="background-image:linear-gradient(black, black),url('<?php echo get_the_post_thumbnail_url(get_the_ID(),'alternating'); ?>');">
											<h2><?php the_title(); ?></h2>
										</div>
									</a>
								</div>
							<?php endforeach; ?>
							<?php wp_reset_postdata(); wp_reset_query();
						} else {

							if ($img && $link !== '' && $title) {
								echo '<div class="two-col-img-container col-md-6"><a href="' . $linkUrl . '" target="' . $linkTarget . '"><div class="two-col-img d-flex align-items-center" style="background-image:url(' . $imgUrl . ');"><h2>' . $title . '</h2></div></a>';
							} else if ($img  && $title) {
								echo '<div class="two-col-img-container col-md-6"><div class="two-col-img d-flex align-items-center" style="background-image:url(' . $imgUrl . ');"><h2>' . $title . '</h2></div>';
							} else if ($img) {
								echo '<div class="two-col-img-container col-md-6"><div class="two-col-img d-flex align-items-center" style="background-image:url(' . $imgUrl . ');"></div>';
							}
								echo '</div>';
						}

					endwhile;
				endif;
			?>
		</div>
	</div>
	<div class="clearfix"></div>
</section>