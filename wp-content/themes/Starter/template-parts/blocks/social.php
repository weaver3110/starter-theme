<?php
/**
 * Block Name: Social Component
 *
 * This is the template that displays the testimonial block.
 */
// create id attribute for specific styling
$id = 'block-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';
?>
<section class="social" id="<?php echo $id; ?>">
    <div class="social-container">
        <div class="container">
            <div class="row text-center social_news_section">
                <div class="col-sm-12"><div class="title"><?php echo _("What's New"); ?></div></div>
                <?php
                    // The Query
                    $args = array( 'max_num_pages' => '1', 'posts_per_page' => '3', 'post_type' => 'post', 'post_status' => 'publish' );
                    $the_query = new WP_Query( $args );
                    
                    // The Loop
                    if ( $the_query->have_posts() ) {
                        while ( $the_query->have_posts() ) {
                            $the_query->the_post();
                            echo '<div class="col-sm text-left">';
                                include(locate_template('template-parts/include--post.php'));
                            echo '</div> ';
                        }
                    } else {
                        // no posts found
                    }
                    /* Restore original Post Data */
                    wp_reset_postdata();
                ?>
                <div class="col-sm-12"><a href="/about-us/news/" class="btn full_news"><?php echo _('View Full List'); ?></a></div>
            </div>
            <div class="row row-eq-height">
                <div class="col-sm social_wrap">
                    <!-- FACEBOOK -->
                    <div id="fb-root"></div>
                    <script>(function(d, s, id) {
                        var js, fjs = d.getElementsByTagName(s)[0];
                        if (d.getElementById(id)) return;
                        js = d.createElement(s); js.id = id;
                        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=158618107542827";
                        fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));
                    </script>
                    <div class="fb-page" data-href="https://www.facebook.com/ToolBankUSA" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                        <blockquote cite="https://www.facebook.com/ToolBankUSA" class="fb-xfbml-parse-ignore">
                            <a href="https://www.facebook.com/ToolBankUSA">ToolBankUSA</a>
                        </blockquote>
                    </div>
                    <div class="social_button"><a href="https://www.facebook.com/ToolBankUSA" target="_blank" class="social_link btn"><?php echo _('View More'); ?></a></div>
                </div>

                <div class="col-sm social_wrap">
                    <!-- TWITTER -->
                    <a class="twitter-timeline" data-height="498" href="https://twitter.com/toolbankusa">Tweets by toolbankusa</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                    <div class="social_button"><a href="https://twitter.com/toolbankusa" target="_blank" class="social_link btn"><?php echo _('View More'); ?></a></div>
                </div>

                <div class="col-sm social_wrap">
                    <!-- YOUTUBE -->
                    <iframe class="you_tube_frame" width="100%" height="315" src="https://www.youtube.com/embed/videoseries?list=PLsnJ1Ps4CQWAOaIyqSVbcMizLT5upQqK3" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    <div class="social_button"><a href="https://www.youtube.com/watch?v=n7bKBf1jsE0" target="_blank" class="social_link btn"><?php echo _('View More'); ?></a></div>
                </div>
            
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</section>