<?php
/**
 * Block Name: Tabbed Component
 *
 * This is the template that displays the testimonial block.
 */
// create id attribute for specific styling
$id = 'block-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';
?>
<section class="tabbed" id="<?php echo $id; ?>">
    <div class="container">
        <?php if (get_field('title')): ?>
            <div class="title"><?php echo get_field('title'); ?></div>
        <?php endif ?>
    <?php 
    if( have_rows('tab') ):
        $i = 0;
        $tab_output = '';
        $content_output = '';
        global $post;
        while ( have_rows('tab') ) : the_row();
            $tab_title = get_sub_field('tab_title');
            $posts = get_sub_field('posts');
            
            if($i == 0){
                $active_class = 'active show';
            } else {
                $active_class = '';
            }

            $tab_output .= '<li><a data-toggle="tab" href="#tab_'.$i.'" class="'.$active_class.'">'.$tab_title.'</a></li>';

            $content_output .= '<div id="tab_'.$i.'" class="tab-pane fade in '.$active_class.'">';
            $content_output .= '<div class="row">';
            foreach($posts as $post):
                $featured_image = get_the_post_thumbnail($post->ID, 'alternating');
                $title = get_the_title($post->ID);
                $button = get_permalink($post->ID);
                $post_type = get_post_type($post->ID);
                if($post_type == 'tb_bios'):
                    $job_title = get_field('job_title', $post->ID);
                    $secondary_image = get_field('secondary_image', $post->ID);
                    $content = apply_filters( 'the_content', get_post_field( 'post_content', get_option( 'page_for_posts' ) ) );
                    $content = str_replace('&','&amp;',$content);
                    $content = str_replace('<','&lt;',$content);
                    $content = str_replace('>','&gt;',$content);
                    $content = str_replace('"','&quot;',$content);
                    $button = '#'.$post->ID;
                endif;

                $content_output .= '<div class="tabbed-post-wrap col-sm-4 '.$post_type.'" id="'.$post->ID.'">';

                    if($featured_image): 
                        $content_output .= '<div class="featured_image">';
                            $content_output .= $featured_image;
                        $content_output .= '</div>';
                    endif;

                    if($title):
                        $content_output .= '<div class="text_title">';
                            $content_output .= $title;
                        $content_output .= '</div>';
                    endif;

                    if( $button ): 
                        $content_output .= '<div class="text_link">';
                            $content_output .= '<a href="'.$button.'"  class="button feature--button tabbed-dd '.$post_type.'" data-id="open_'.$post->ID.'"';
                                if($secondary_image):
                                $content_output .= ' data-img-src="'.$secondary_image['url'].'" data-img-alt="'.$secondary_image['alt'].'" data-img-title="'.$secondary_image['title'].'"';
                                endif;
                                if($title):
                                    $content_output .= ' data-title="';
                                    $content_output .= $title;
                                    $content_output .= '"';
                                endif;

                                if($job_title):
                                    $content_output .= ' data-job-title="';
                                    $content_output .= $job_title;
                                    $content_output .= '"';
                                endif;

                                if($content):
                                    $content_output .= ' data-content="';
                                    $content_output .= $content;
                                    $content_output .= '"';
                                endif;
                                $content_output .= '>';
                                if($post_type != 'tb_bios'):
                                    $content_output .= _('Read More'); 
                                endif;
                            $content_output .= '</a>';
                        $content_output .= '</div>';

                    endif;

                    if($post_type == 'tb_bios'):
                        if($job_title):
                            $content_output .= '<div class="text_job_title">';
                                $content_output .= $job_title; 
                            $content_output .= '</div>';
                        endif;

                        // $content_output .= '<div class="slide" id="open_'.$post->ID.'">';
                        //     $content_output .= '<div class="close">X</div>';
                            
                        //     if($secondary_image):
                        //         $content_output .= '<div class="secondary_image">';
                        //         $content_output .= '<img src="'.$secondary_image['url'].'" alt="'.$secondary_image['alt'].'" title="'.$secondary_image['title'].'">';
                        //         $content_output .= '</div>';
                        //     endif;
                        //     $content_output .= '<div class="slide-content-wrap">';
                        //         if($title):
                        //             $content_output .= '<div class="text_title h4">';
                        //             $content_output .= $title; 
                        //             $content_output .= '</div>';
                        //         endif;

                        //         if($job_title):
                        //             $content_output .= '<div class="text_job_title h6">';
                        //             $content_output .= $job_title; 
                        //             $content_output .= '</div>';
                        //         endif;

                        //         if($content):
                        //             $content_output .= '<div class="text_content">';
                        //             $content_output .= $content; 
                        //             $content_output .= '</div>';
                        //         endif;
                                
                        //         $content_output .= '<div class="clearfix"></div>';
                        
                        //     $content_output .= '</div>';

                        // $content_output .= '</div>';
                    endif;

                $content_output .= '</div>';

            endforeach;
            $content_output .= '</div>';
            $content_output .= '</div>';
        $i++;        
        endwhile;
    endif;
    ?>

        <ul class="nav search_tabs">
            <?php echo $tab_output; ?>
        </ul>

        <div class="tab-content">
            <?php echo $content_output; ?>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="clearfix"></div>
</section>