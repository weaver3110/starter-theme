<?php 
/**
 * Block Name: Content Component
 *
 * This is the template that displays the testimonial block.
 */
// create id attribute for specific styling
$id = 'block-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

//vars
$bgType = get_field('background_type');
$bgImg = get_field('background_image');
$bgColor = get_field('background_color');

if ($bgType == 'image') {
	$bg = ' no_padding bg-img" style="background-image:linear-gradient(black, black), url(' . $bgImg["url"] . ');';
} else if ($bgType == 'color') {
	$bg = ' '.$bgColor;
} else {
	$bg = '';
}

$title = get_field('banner_title');
$content = get_field('banner_content');

$link = get_field('cta_link');
if($link) {
	$linkTitle = $link['title'];
	$linkTarget = $link['target'];
	$linkUrl = $link['url'];
}
$formId = get_field('form');
?>


<section class="content<?php echo $bg; ?>" id="<?php echo $id; ?>">
<!-- markup php -->
<?php 
	if ($bgType == 'image') {
		echo '<div class="content-overlay">';
	} 

	echo '<div class="container">';
		echo '<div class="content-content">';

		$contentTitle = get_field('content_title');
		$content = get_field('content');

		echo '<div class="content-row">';

		if ($contentTitle) {
			echo '<div class="h6 title">' . $contentTitle . '</div>';
		}
		if ($content) {
			echo '<div class="content-block">' . $content . '</div>';
		}
		?>
		</div>
	</div>
	<?php if ($bgType == 'image') {
		echo '</div>';
	} ?>
</div>

</section>