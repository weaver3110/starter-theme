<?php
/**
 * Block Name: Alternating Component
 *
 * This is the template that displays the Alternating block.
 */
// create id attribute for specific styling
$id = 'block-' . $block['id'];

?>
<section class="alternating" id="<?php echo $id; ?>">
	<?php if (get_field('title')): ?>
		<div class="title"><?php echo get_field('title'); ?></div>
	<?php endif ?>
	<?php 
	/***************
	 * *************
	 * AUTO
	 * *************
	****************/
	//vars
	$importAuto = get_field('import_posts_automatically');

	if ($importAuto) {
		//vars
		$post_type = get_field('post_type');
		$args = array(
			'post_type' 		=> $post_type,
			'posts_per_page' 	=> '3',
			'orderby'			=> 'date'
		);
		$block_query = new WP_Query($args);
		global $post;
		$blockCount = $block_query->post_count;
		// The Loop
		if ( $block_query->have_posts() ) :
			$i = 0;
			while ( $block_query->have_posts() ) :
				$i++;
				if ($i% 2 == 0){
					$altClass = ' ';
				}
				else {
					$altClass = '';
				}
				$block_query->the_post();
				setup_postdata($post);
				$post_type = get_post_type();
				$imgType = $post_type;
				$img = get_the_post_thumbnail($post->ID, 'alternating');
				$vid = get_field('video', $post->ID);
				$title = get_the_title($post->ID);
				$alt = get_the_excerpt($post->ID);
				$link = get_permalink($post->ID);
				$linkTitle = 'Learn More';
				$linkTarget = '_self';
				$linkUrl = $link;
				echo '<div class="alternating-row container" data-type="auto">';
					echo '<div class="row">';
						if ($imgType != 'tb_video') {
							echo '<div class="altImg '.$altClass.'">';
							echo $img;
							echo '';
							echo '</div>';
						} else {
							echo '<div class="altVideo '.$altClass.'">';
								$vid;
							echo '</div>';
						}
						echo '<div class="altText-container ">';
							echo '<h2>' . $title . '</h2>'; 
							echo '<div class="altText">' . $alt . '</div>'; 
							echo '<a href="' . $linkUrl . '" target="' . $linkTarget . '" class="btn">' . $linkTitle . '</a>';
						echo '</div>';
					echo '</div>';
					echo '<div class="clearfix"></div>';
				echo '</div>';
				endwhile;
			/* Restore original Post Data */
			wp_reset_postdata(); wp_reset_query();
		endif;
	} else {
		if( have_rows('alternating') ):
			$i = 0;
			while ( have_rows('alternating') ) : the_row();
				$i++;
				if ($i% 2 == 0){
					$altClass = ' ';
				}
				else {
					$altClass = '';
				}
				$importPosts = get_sub_field('import_from_posts');
				$imgType = get_sub_field('image_type');
				$img = get_sub_field('image');
				$vid = get_sub_field('video');
				$title = get_sub_field('title');
				$alt = get_sub_field('alternating_copy');
				$link = get_sub_field('link');
				if ($link) {
					$linkTitle = $link['title'];
					$linkTarget = $link['target'];
					$linkUrl = $link['url'];
				}
				$imgUrl = $img['url'];
				$imgAlt = $img['alt'];

				/***************
				 * *************
				 * MANUAL
				 * *************
				****************/
				if ($importPosts) {
					$manually_add_posts = get_sub_field('choose_posts_to_show');
					global $post;
							foreach( $manually_add_posts as $post):
								setup_postdata($post);
								$post_type = get_post_type();
								$imgType = $post_type;
								$img = get_the_post_thumbnail($post->ID, 'alternating');
								$vid = get_field('video', $post->ID);
								$title = get_the_title($post->ID);
								$alt = get_the_excerpt($post->ID);
								$link = get_permalink($post->ID);
								$linkTitle = 'Learn More';
								$linkTarget = '_self';
								$linkUrl = $link; ?>
										<?php 
										echo '<div class="alternating-row container" data-type="manual">';
											echo '<div class="row">';
												if ($imgType != 'tb_video') {
													echo '<div class="altImg '.$altClass.'">';
													echo $img;
													echo '<div class="clearfix"></div>';
													echo '</div>';
												} else {
													echo '<div class="altVideo '.$altClass.'">';
														$vid;
													echo '</div>';
												}
												echo '<div class="altText-container ">';
													echo '<h2>' . $title . '</h2>'; 
													echo '<div class="altText">' . $alt . '</div>'; 
													echo '<a href="' . $linkUrl . '" target="' . $linkTarget . '" class="btn">' . $linkTitle . '</a>';
												echo '</div>';
											echo '</div>';
											echo '<div class="clearfix"></div>';
										echo '</div>';
								endforeach;
					wp_reset_postdata(); wp_reset_query(); 
				} else {
				/***************
				 * *************
				 * STATIC
				 * *************
				****************/				
					echo '<div class="alternating-row container" data-type="static">';
						echo '<div class="row">';
							if ($imgType == 'image') {
								echo '<div class="altImg "><img src="' . $imgUrl . '" alt="' . $imgAlt . '"/></div>';
							} else {
								echo '<div class="altVideo ">';
									$vid;
								echo '</div>';
							}
							echo '<div class="altText-container ">';
								if ($title) {
										echo '<h2>' . $title . '</h2>';
								}
								if ($alt) {
									echo '<div class="altText">' . $alt . '</div>'; 
								}
								if ($link) {
									echo '<a href="' . $linkUrl . '" target="' . $linkTarget . '" class="btn">' . $linkTitle . '</a>';
								}
							echo '</div>';
						echo '</div>';
						echo '<div class="clearfix"></div>';
					echo '</div>';
				}
			endwhile;
		endif;
	}
	?>
<div class="clearfix"></div>
</section>