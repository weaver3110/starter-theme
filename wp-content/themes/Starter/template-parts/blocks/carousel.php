<?php
/**
 * Block Name: Bootstrap Carousel
 *
 * This is the template that displays the testimonial block.
 */
// create id attribute for specific styling
$id = 'testimonial-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

?>
<section class="carousel" id="<?php echo $id; ?>">
    <div id="carousel-<?php echo $id; ?>" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <?php
        $i = 0;
        if( have_rows('images') ):
            while ( have_rows('images') ) : the_row();
            if($i == 0){
                $class = 'class="active"';
            }else{
                $class = '';
            }
            ?>
            <li data-target="#carousel-<?php echo $id; ?>" data-slide-to="<?php echo $i; ?>" <?php echo $class; ?>></li>
            <?php
            $i++;
            endwhile;
        endif;
        ?>
      </ol>
      <div class="carousel-inner">
      <?php
        $i = 0;
        if( have_rows('images') ):
            while ( have_rows('images') ) : the_row();
            if($i == 0){
                $class = 'active';
            }else{
                $class = '';
            }
            $img = get_sub_field('image');
            $imgUrl = $img['url'];
            $imgAlt = $img['alt'];
            ?>
            <div class="carousel-item <?php echo $class; ?>">
                <img class="d-block w-100" src="<?php echo $imgUrl; ?>" alt="<?php echo $imgAlt; ?>">
            </div>
            <?php
            $i++;
            endwhile;
        endif;
        ?>
      </div>
      <a class="carousel-control-prev" href="#carousel-<?php echo $id; ?>" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carousel-<?php echo $id; ?>" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>


</section>