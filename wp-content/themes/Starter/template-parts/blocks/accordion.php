<?php
/**
 * Block Name: Alternating Component
 *
 * This is the template that displays the testimonial block.
 */
// create id attribute for specific styling
$id = 'block-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';
$i = 0;
?>
<section class="accordion" id="<?php echo $id; ?>">
	<div class="container">
		<?php if (get_field('title')): ?>
			<div class="title"><?php echo get_field('title'); ?></div>
		<?php endif ?>
		<div id="accordion">
			<?php 
			if( have_rows('accordion') ):
				while ( have_rows('accordion') ) : the_row();
					$question = get_sub_field('question');
					$answer = get_sub_field('answer');
					$link = get_sub_field('link');
					if ($link) {
						$linkTitle = $link['title'];
						$linkTarget = $link['target'];
						$linkUrl = $link['url'];
					}
				?>
				<div class="card">
					<div class="card-header" id="acc-<?php echo $i; ?>">
						<div class="mb-0" type="toggle" data-toggle="collapse" data-target="#coll-<?php echo $i; ?>" aria-expanded="flase" aria-controls="coll-<?php echo $i; ?>" role="button">
							<?php echo $question; ?> <i class="fa fa-angle-down rotate-icon"></i>
						</div>
					</div>

					<div id="coll-<?php echo $i; ?>" class="collapse" aria-labelledby="acc-<?php echo $i; ?>" data-parent="#accordion">
						<div class="card-body">
							<div class="card-body-content">
								<?php 
									if ($answer) {
										echo $answer;
									}
									if ($link) {
										echo '<a href="' . $linkUrl . '" target="' . $linkTarget . '">' . $linkTitle . '</a>';
									}
								?>
							</div>
						</div>
					</div>
				</div>
			<?php
				$i++;
				endwhile;
			endif;
			?>
		</div>
	</div>
<div class="clearfix"></div>
</section>