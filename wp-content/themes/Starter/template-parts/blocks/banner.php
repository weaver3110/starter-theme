<?php 
/**
 * Block Name: Banner Component
 *
 * This is the template that displays the testimonial block.
 */
// create id attribute for specific styling
$id = 'block-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

//vars
$bgType = get_field('background_type');
$bgImg = get_field('background_image');
$bgColor = get_field('background_color');

if ($bgType == 'image') {
	$bg = ' no_padding bg-img" style="background-image:linear-gradient(black, black), url(' . $bgImg . ');"';
} else if ($bgType == 'color') {
	$bg = ' '.$bgColor;
} else {
	$bg = '';
}

$title = get_field('banner_title');
$content = get_field('banner_content');
$banType = get_field('banner_type');
$link = get_field('cta_link');
if($link) {
	$linkTitle = $link['title'];
	$linkTarget = $link['target'];
	$linkUrl = $link['url'];
}
$formId = get_field('form');
?>

<section class="banner<?php echo $bg.' '.$banType; ?>" id="<?php echo $id; ?>">

 <!-- markup php -->
<div class="banner-row">
<?php 
	if ($bgType == 'image') {
		echo '<div class="banner-overlay '.$banType.'">';
	} 

	echo '<div class="container">';
		echo '<div class="banner-content">';

			if ($title) {
				echo '<div class="h6 title">' . $title . '</div>';
			}

			if ($content) {
				echo '<div class="content-block">' . $content . '</div>';
			}

			if ($banType == 'cta' && $link != '') {
				echo '<div class="banner-cta text-center">';
					echo '<a href="' . $linkUrl . '" target="' . $linkTarget . '" class="btn orange">' . $linkTitle . '</a>';
				echo '</div>';
			} elseif ($banType == 'form') {
				echo '<div class="banner-form">';
					echo '[gravityform id=' . $formId . ' title=false description=false ajax=true tabindex=49]';
				echo '</div>';
			}
?>
		</div>
	</div>
	<?php if ($bgType == 'image') {
		echo '</div>';
	} ?>
</div>

</section>