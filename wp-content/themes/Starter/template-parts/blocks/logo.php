<?php
/**
 * Block Name: Logo Component
 *
 * This is the template that displays the testimonial block.
 */
// create id attribute for specific styling
$id = 'block-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

?>
<section class="logo" id="<?php echo $id; ?>">
	<div class="container">
		<div class="title"><?php echo _('TOOLBANK INVESTORS'); ?></div>
		<div id="<?php echo $id; ?>" class="logo row">

		 <!-- markup php -->
		<?php 
			$img = get_field('logo');
			$count = 0;
			if($img):
				foreach ($img as $post) {
					setup_postdata($post);
					if ($count < 4) {
						include(locate_template('template-parts/include--tb_donors.php'));
					} else {}
					$count++;
				}
			endif;
		?>
		</div>
		<?php
			if ($count >= 4 && !is_page( 'donate' )) {
		?>
				<div class="container">
		    		<div class="row justify-content-center">
		    			<div class="col-md-12 logo_more"><a href="/donate/" class="btn">View More</a></div>
		    		</div>
		    	</div>
		<?php
		    }
		?>
			
		<div class="clearfix"></div>
	</div>
</section>