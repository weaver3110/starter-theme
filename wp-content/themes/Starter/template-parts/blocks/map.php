<?php
/**
 * Block Name: Map Component
 *
 * This is the template that displays the testimonial block.
 */
// create id attribute for specific styling
$id = 'block-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';
?>
<section class="map" id="<?php echo $id; ?>">
<div class="map-container">
    <div class="map-title"><?php the_field('title'); ?></div>
    <iframe src="https://www.google.com/maps/d/embed?mid=1Bn5c8q2zW_v6X3FGBCzOcMw51YI" style="width:100%;height:80vh"></iframe>
</div>
<div class="clearfix"></div>
</section>