<?php
/*
* Template Name: News List Page
*/
get_header();
?>

<div class="wrapper">
	<?php
		if ( have_posts() ) {
			while ( have_posts() ) {
				the_post(); 
				the_content();
			} // end while
		} // end if
		?>
	<section class="news_list">
		<div class="container">
			<div class="row">
			<?php
				$args = array( 
					'post_type' => 'post' 
				);
				// The Query
				$the_query = new WP_Query( $args );
				
				// The Loop
				if ( $the_query->have_posts() ) {
					echo '<div class="container">
						<div class="row">';
							while ( $the_query->have_posts() ) {
								$the_query->the_post();
								setup_postdata($post);
								$post_type == 'post';
							?>
								<div class="col col-sm-4" data-type="<?php echo $post_type; ?>_auto" data-position="<?php echo $blockCount; ?>">
									<?php include(locate_template('template-parts/include--post.php')); ?>
								</div>

							<?php }
						echo '</div>
					</div>';
				} else {
					// no posts found
				}
				/* Restore original Post Data */
				wp_reset_postdata();
			?>
			</div>
		</section>
	</div>
</div>

<?php get_footer(); ?>