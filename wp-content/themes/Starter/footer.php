<footer class="global-footer">
	<div class="container">
		<div class="row">
			<?php
				wp_nav_menu([
				'menu'            => 'Footer Navigation',
				'theme_location'  => 'footer',
				]);
			?>
			<div class="footer-social">
				<span class="connect"><?php echo _('Connect With Us'); ?></span><br>
				<a href="#" target="_blank"><i class="fa fa-facebook-f"></i></a>
				<a href="#" target="_blank"><i class="fa fa-linkedin"></i></a>
				<a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
				<a href="#" target="_blank"><i class="fa fa-youtube-play"></i></a>
			</div>
		</div>
	</div>
	<div class="footer-second">
		<?php echo _('Copyright '.date("Y").' ToolBank USA'); ?>
	</div>
</footer>

<?php if( is_user_logged_in() && current_user_can( 'edit_posts' ) ) { ?>
<nav class="logged-in-nav">
	<ul>
		<li>Logged In:</li>
		<li><a href="<?php echo admin_url(); ?>">Admin</a></li>
		<?php edit_post_link( __( 'Edit Post' ), '<li>', '</li>' );?>
	</ul>
</nav>
<?php }; ?>

<?php wp_footer(); ?>
<?php echo get_field('end_scripts', 'options'); ?>
</body>
</html>